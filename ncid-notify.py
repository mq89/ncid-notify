#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import datetime
import notify2
import os
import pprint
import re
import socket
import sys
import yaml

# Version string
VERSION = "0.1.0"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="Config file to use.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Be verbose.")
    parser.add_argument("-V", "--version", action="store_true", help="Show version and exit.")

    args = parser.parse_args()

    # print version information and exit.
    if args.version:
        print("ncid-notify v{}".format(VERSION))
        sys.exit(0)

    # default config
    config = {
        "server": "localhost",
        "port": 3333
    }

    default_config = "ncid-notify-default.yml"

    if os.path.exists(default_config):
        with open(default_config) as f:
            config.update(yaml.load(f, Loader=yaml.Loader))
        print("Loaded {}".format(default_config))

    # overwrite default config if config file is provided
    if args.config is not None:
        if os.path.exists(args.config):
            with open(args.config) as f:
                config.update(yaml.load(f, Loader=yaml.Loader))
            print("Loaded {}".format(args.config))
        else:
            print("Can not find \"{}\"".format(args.config))
            sys.exit(1)

    # print config
    if args.verbose:
        print("config:")
        pprint.pprint(config)

    # open connection
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        connection.connect((config["server"], config["port"]))
    except ConnectionRefusedError:
        print("Can not connect to {}:{}".format(config["server"], config["port"]))
        sys.exit(2)

    # load notify
    notify2.init('ncid-notify')
    notification = notify2.Notification("NCID Notify v{}".format(VERSION), "Connected to: {}:{}".format(
        config["server"], config["port"]), "phone")
    notification.show()

    # pattern to find phone number in ncid
    pattern = re.compile(r"\*NMBR\*(.*)\*MESG\*")

    # control loop
    repeat = True
    while repeat:
        try:
            response = connection.recv(4096).decode("utf-8").strip()

            # only consider lines starting with "CID:"
            if response[0:4] == "CID:":
                result = pattern.search(response)
                if result:
                    # find phone number
                    number = result.group(1)

                    # strip German country code
                    if number[0:3] == "+49":
                        number = number.replace("+49", "0")

                    # create notification
                    try:
                        notify2.init('ncid-notify')
                        notification = notify2.Notification("Anruf", number, "phone")
                        notification.show()
                        if args.verbose:
                            print("{}: Display notification: {}".format(
                                datetime.datetime.now().isoformat()[:19], number))
                    except:
                        print("{}: {}".format(datetime.datetime.now().isoformat()[:19], sys.exc_info()[0]))
                        print("{}".format(number))
            elif args.verbose:
                print("Ignore line: {}".format(response))

        except KeyboardInterrupt:
            connection.shutdown(socket.SHUT_WR)
            connection.close()
            repeat = False


if __name__ == '__main__':
    main()
