# NCID Notify

Uses the NCID protocol to fetch the caller ID of an incoming call and displays it as a notificiation via libnotify.

## Dependencies

- Requires packagekit: `apt get install packagekit`

## Configuration

Server and port can be configured via a yaml file.

The default values are:

```yaml
server: "localhost"
port: 3333
```

The configuration file named `ncid-notify-default.yml` is loaded automatically if it exists and overwrites the default values.
Any yaml file provided by the `-c` argument is loaded and overwrites the previous configuration.

## Todo

- Document dependencies.
- Document configuration.
- Implement phone number lookup in addressbook.
