# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Catch any error while creating notification.
- Catch GLib.GError and try re-initializing the notification.

### Changed

- Use notify2 instead of gi.repository.Notify

## [0.1.0] - 2020-05-23

### Added

- Create initial version using NCID and libnotify.

[Unreleased]: https://gitlab.com/Mq_/ncid-notify/compare/v0.1.0...master
[0.1.0]: https://gitlab.com/Mq_/ncid-notify/tags/v0.1.0
